<?php
function limit_text($text, $limit) {

    if (str_word_count($text, 0) > $limit) {
        $text = html_entity_decode(strip_tags($text));
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
      return $text;
}

function tipe_text($text) {

        $text = strip_tags($text);
        // $words = str_word_count($text, 2);
    
      return $text;
}

function random($length)
{
    $data = 'ABCDEFGHIJKLMNOPQRSTU1234567890';
    $string = '';
    for($i = 0; $i < $length; $i++) {
        $pos = rand(0, strlen($data)-1);
        $string .= $data{$pos};
    }
    return $string;
}

function randomImage($length)
{
    $data = '1234567890';
    $string = '';
    for($i = 0; $i < $length; $i++) {
        $pos = rand(0, strlen($data)-1);
        $string .= $data{$pos};
    }
    return $string;
}

function createImage($path, $filename, $id, $proporsional = false) {
    $newFileName = urlParsing($filename);
    $big = $path . $id . 'main_' . $newFileName;
    $thumb = $path . $id . 'thumb_'. $newFileName;
    if (file_exists($big) && file_exists($thumb)) {
        // unlink($big);
        // unlink($thumb);
    }
    $file = $path . $filename;

    smart_resize_image($file, $big, config('resize_image_width_big'), config('resize_image_height_big'), true , 90);
    smart_resize_image($file, $thumb, config('resize_image_width_thumb'), config('resize_image_height_thumb'), true , 90);

    // move_uploaded_file($file);
    return [
        'big' => $id . 'main_' . $newFileName,
        'thumb' => $id . 'thumb_' . $newFileName
    ];
}

function urlParsing($string) {
    $arrDash = array("--", "---", "----", "-----");
    $string = strtolower(trim($string));
    $string = strtr($string, normalizeChars());
    $string = preg_replace('/[^a-zA-Z0-9 -.]/', '', $string);
    $string = str_replace(" ", "-", $string);
    $string = str_replace("&", "", $string);
    $string = str_replace(array("'", "\"", "&quot;"), "", $string);
    $string = str_replace($arrDash, "-", $string);
    return str_replace($arrDash, "-", $string);
}

function countArtikel($status,$id){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $db->select('count(*) as jumlah')
        ->from('artikel');

    if ($status == 'Penulis') {
        $db->where("created_by","=",$id);
    } else {
        $db->where("modified_by","=",$id);
    }
    return $db;
}

function getDescription($content){
    $awal = explode("<br />", $content);
    return strip_tags($awal[5]);
}

function getPortofolioap(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("artikel.*, artikel_kategori.nama as nama_kategori")
        ->from("artikel")
        ->join("LEFT JOIN","artikel_kategori","artikel.kategori = artikel_kategori.id")
        ->where("status", "=", "publish") 
        ->orderBy("artikel.id DESC")
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
      $judul = $value->judul;
      $value->gambar_thumb = gambar_pertama($value->isi_content);
      $value->tgl_berita = date('d', $value->created_at );
      $value->bulan_berita = date('F', $value->created_at );
      $value->created_at = date('d F Y', $value->created_at );
    }
    return $contentbox;
}
function latestPortofolio(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $contentbox = $db->select("*")
        ->from("artikel")
        ->where("status", "=", "publish")
        ->limit(5)
        ->orderBy("artikel.id DESC")
        ->findAll();

    // Portofolio
    foreach ($contentbox as $key => $value) {
      $judul = $value->judul;
      $value->gambar_thumb = gambar_pertama($value->isi_content);
      $value->tgl_berita = date('d', $value->created_at );
      $value->bulan_berita = date('F', $value->created_at );
      $value->created_at = date('d F Y', $value->created_at );
    }
    
    return $contentbox;
}

//fungsi baru
function gambar_pertama($string, $default = "app/img/no-image-article.png") {
    // preg_match('@<img.+src="(.)".>@Uims', $string, $matches);

    preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $string, $image);
    // $src = $matches ? $matches[1] : site_url() .$default;
    return @$image['src'];
    // return $src;
}

function getSettingweb(){
    $db = new Cahkampung\Landadb(config('DB')['db']);

    $get_sw = $db->select("*")
        ->from("setting")
        ->findAll();

    return $get_sw;
}
