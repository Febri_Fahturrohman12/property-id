angular.module("app").config(["$stateProvider", "$urlRouterProvider", "$ocLazyLoadProvider", "$breadcrumbProvider",
    function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {
        $urlRouterProvider.otherwise("/dashboard");
        $ocLazyLoadProvider.config({
            debug: false
        });
        $breadcrumbProvider.setOptions({
            prefixStateName: "app.main",
            includeAbstract: true,
            template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
        });
        $stateProvider.state("app", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Root",
                skip: true
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ],
            }
        }).state("app.main", {
            url: "/dashboard",
            templateUrl: "tpl/dashboard/dashboard.html",
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js", 'naif.base64']).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/dashboard/dashboard.js"]
                            });
                        });
                    }
                ]
            }
        }).state("app.generator", {
            url: "/generator",
            templateUrl: "tpl/generator/index.html",
            ncyBreadcrumb: {
                label: "Home"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["chart.js"]).then(() => {
                            return $ocLazyLoad.load({
                                files: ["tpl/generator/index.js"]
                            });
                        });
                    }
                ]
            }
        }).state("pengguna", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "User Login"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        }).state("pengguna.akses", {
            url: "/hak-akses",
            templateUrl: "tpl/m_akses/index.html",
            ncyBreadcrumb: {
                label: "Hak Akses"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_akses/index.js"]
                        });
                    }
                ]
            }
        }).state("pengguna.user", {
            url: "/user",
            templateUrl: "tpl/m_user/index.html",
            ncyBreadcrumb: {
                label: "Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/index.js"]
                        });
                    }
                ]
            }
        }).state("pengguna.profil", {
            url: "/profil",
            templateUrl: "tpl/m_user/profile.html",
            ncyBreadcrumb: {
                label: "Profil Pengguna"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_user/profile.js"]
                        });
                    }
                ]
            }
        }).state("page", {
            abstract: true,
            templateUrl: "tpl/common/layouts/blank.html",
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon"]);
                    }
                ]
            }
        }).state("page.login", {
            url: "/login",
            templateUrl: "tpl/common/pages/login.html",
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/site/login.js"]
                        });
                    }
                ]
            }
        }).state("page.404", {
            url: "/404",
            templateUrl: "tpl/common/pages/404.html"
        }).state("page.500", {
            url: "/500",
            templateUrl: "tpl/common/pages/500.html"
        }).state("pm", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Pengaturan Menu"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        }).state("pm.menu", {
            url: "/pengaturan-menu",
            templateUrl: "tpl/m_menu/index.html",
            ncyBreadcrumb: {
                label: "Keywords Dan Viral Minggu Ini"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_menu/index.js"]
                        });
                    }
                ]
            }
        }).state("pm.migrasi", {
            url: "/migrasi-data",
            templateUrl: "tpl/m_migrasi/index.html",
            ncyBreadcrumb: {
                label: "Migrasi Data"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/m_migrasi/index.js"]
                        });
                    }
                ]
            }
        }).state("master", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Master"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        }).state("master.gallery", {
            url: "/master-gallery",
            templateUrl: "tpl/m_galeri/index.html",
            ncyBreadcrumb: {
                label: "Gallery"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/m_galeri/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("master.kategori_gallery", {
            url: "/kategri-gallery",
            templateUrl: "tpl/m_galeri_kategori/index.html",
            ncyBreadcrumb: {
                label: "Kategori Gallery"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/m_galeri_kategori/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("master.testimoni", {
            url: "/tesimoni",
            templateUrl: "tpl/m_testimoni/index.html",
            ncyBreadcrumb: {
                label: "Testimoni"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/m_testimoni/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("master.kategori_artikel", {
            url: "/kategori-artikel",
            templateUrl: "tpl/m_artikel_kategori/index.html",
            ncyBreadcrumb: {
                label: "Kategori Artikel"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/m_artikel_kategori/index.js']
                          });
                        });
                    }
                ]
            }
        })
        .state("master.artikel", {
            url: "/master-artikel",
            templateUrl: "tpl/artikel/allbaru.html",
            ncyBreadcrumb: {
                label: "Kategori Artikel"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/indexbaru.js']
                          });
                        });
                    }
                ]
            }
        })
        .state("master.setting", {
            url: "/master-setting",
            templateUrl: "tpl/m_setting/setting.html",
            ncyBreadcrumb: {
                label: "Setting"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/m_setting/setting.js']
                          });
                        });
                    }
                ]
            }
        }).state("master.download", {
            url: "/master-download",
            templateUrl: "tpl/m_download/index.html",
            ncyBreadcrumb: {
                label: "File Download"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/m_download/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("artikel", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Artikel"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        }).state("artikel.all", {
            url: "/artikel-all",
            templateUrl: "tpl/artikel/allbaru.html",
            ncyBreadcrumb: {
                label: "Artikel All"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/indexbaru.js']
                          });
                        });
                    }
                ]
            }
        }).state("artikel.editorial", {
            url: "/artikel-editorial",
            templateUrl: "tpl/artikel/editorialbaru.html",
            ncyBreadcrumb: {
                label: "Artikel Editorial"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/indexbaru.js']
                          });
                        });
                    }
                ]
            }
        }).state("artikel.published", {
            url: "/artikel-published",
            templateUrl: "tpl/artikel/published.html",
            ncyBreadcrumb: {
                label: "Artikel Published"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("artikel.revisi", {
            url: "/artikel-revisi",
            templateUrl: "tpl/artikel/revisi.html",
            ncyBreadcrumb: {
                label: "Artikel Revisi"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("artikel.ditolak", {
            url: "/artikel-ditolak",
            templateUrl: "tpl/artikel/ditolak.html",
            ncyBreadcrumb: {
                label: "Artikel Ditolak"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("artikel.trash", {
            url: "/artikel-trash",
            templateUrl: "tpl/artikel/trash.html",
            ncyBreadcrumb: {
                label: "Artikel Trash"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("artikel.draf", {
            url: "/artikel-draf",
            templateUrl: "tpl/artikel/draf.html",
            ncyBreadcrumb: {
                label: "Artikel Draf"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['naif.base64']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/artikel/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("laporan", {
            abstract: true,
            templateUrl: "tpl/common/layouts/full.html",
            ncyBreadcrumb: {
                label: "Laporan"
            },
            resolve: {
                loadCSS: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(["fontawesome", "simplelineicon", "iconflag"]);
                    }
                ],
                loadPlugin: ["$ocLazyLoad", function($ocLazyLoad) {}],
                authenticate: authenticate
            }
        }).state("laporan.jumlahartikel", {
            url: "/laporan-jumlahartikel",
            templateUrl: "tpl/laporan/jumlahartikel.html",
            ncyBreadcrumb: {
                label: "Bulanan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load(['daterangepicker']).then(() => {
                          return $ocLazyLoad.load({
                              files: ['tpl/laporan/index.js']
                          });
                        });
                    }
                ]
            }
        }).state("laporan.bulanan", {
            url: "/laporan-bulanan",
            templateUrl: "tpl/laporan/bulanan.html",
            ncyBreadcrumb: {
                label: "bulanan"
            },
            resolve: {
                loadMyCtrl: ["$ocLazyLoad",
                    function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: ["tpl/laporan/index.js"]
                        });
                    }
                ]
            }
        });

        function authenticate($q, UserService, $state, $transitions, $location, $rootScope) {
            var deferred = $q.defer();
            if (UserService.isAuth()) {
                deferred.resolve();
                var fromState = $state;
                var globalmenu = ["page.login", "pengguna.profil", "app.main", "page.500", "app.generator"];
                $transitions.onStart({}, function($transition$) {
                    var toState = $transition$.$to();
                    if ($rootScope.user.akses[toState.name.replace(".", "_")] || globalmenu.indexOf(toState.name)) {} else {
                        $state.target("page.500")
                    }
                });
            } else {
                $location.path("/login");
            }
            return deferred.promise;
        }
    }
]);