angular.module("app").controller("AppCtrl", ["$rootScope", "$scope", "UserService", "Data", "$location", function($rootScope, $scope, UserService, Data, $location) {
    $scope.logout = function() {
        UserService.delUser();
        // firebase.auth().signOut().then(function() {
		  	Data.get("site/logout").then(function(response) {
        		$location.path('/login');
    		});
		// }).catch(function(error) {
		//   	console.log(error);
		// });
    };
}]);