app.controller("settingCtrl", function($scope, Data, toaster, UserService) {

  // Get Setting
  Data.get("m_setting/index").then(function(result) {
    if (result.status_code == 200) {
      if(result.data.id != undefined){
        $scope.form         = result.data;
      } else {
        $scope.form         = {};
      }
    } else {
      toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
    }
  });

  $scope.save = function(form) {
    $scope.loading = true;
    Data.post("m_setting/save", form).then(function(result) {
      if (result.status_code == 200) {
        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
      } else {
        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
      }
      $scope.loading = false;
    });
  };
});
