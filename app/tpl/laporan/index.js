app.controller("laporanCtrl", function($scope, Data, toaster) {
    var tableStateRef = {};
    $scope.displayed = [];
    $scope.filter = [];
    $scope.is_view = false;
    $scope.filter.tanggal = {
        startDate : new Date(),
        endDate : new Date()
    }
    var a = new Date();
    var th = a.getFullYear()
    var mn = a.getMonth();
    $scope.filter.bulan = mn.toString();
    $scope.filter.tahun = th.toString();
   
    Data.get("laporan/tahun").then(function(response) {
        $scope.tahun = response.data.list;
    });

    $scope.view = function(data){
        $scope.is_view = true;
        $scope.isLoading = true;
        Data.get("laporan/jumlahartikel", data).then(function(response) {
            $scope.displayed = response.data.list;
            $scope.periode = response.data.periode;
            $scope.total = response.data.total;
        });
        $scope.isLoading = false;
    }

    $scope.view_bulanan = function(data){
        if (data.bulan == "" || data.tahun == "") {
            toaster.pop("error","Terjadi Kesalahan",setErrorMessage(["Bulan dan Tahun Wajib diisi"]));
        } else {
            $scope.is_view = true;
            data.tanggal = new Date(data.tahun,data.bulan);
            $scope.isLoading = true;
            Data.get("laporan/bulanan", {nama:data.nama,tanggal:data.tanggal}).then(function(response) {
                $scope.displayed = response.data.list;
                $scope.periode   = response.data.periode;
                var hari         = response.data.tanggal;
                $scope.totalHari = response.data.tanggal + 1;
                $scope.keseluruhan = response.data.keseluruhan;
                $scope.totalbawah = response.data.totalbawah;
                $scope.hari      = {};
                for (var i = 1; i <= hari; i++) {
                    $scope.hari[i] = i;
                }
            });
            $scope.isLoading = false;
        }
    }

    $scope.reset_filter = function(){
        $scope.filter.nama = "";
        $scope.filter.tanggal = {
            startDate : new Date(),
            endDate : new Date()
        }
        $scope.is_view = false;
    }

    $scope.reset_filter_bulanan = function(){
        $scope.filter.nama = "";
        var a = new Date();
        var th = a.getFullYear()
        var mn = a.getMonth();
        $scope.filter.bulan = mn.toString();
        $scope.filter.tahun = th.toString();
        $scope.is_view = false;
    }

});