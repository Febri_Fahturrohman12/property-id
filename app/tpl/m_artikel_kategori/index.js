app.controller('kategoriArtikelCtrl', function ($scope, Data, toaster) {

    //init data
    var tableStateRef;
    var nmcontroller = "app_kategori_artikel";

    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;


    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {offset: offset, limit: limit};

        if (tableState.sort.predicate) {
            param['sort'] = tableState.sort.predicate;
            param['order'] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param['filter'] = tableState.search.predicateObject;
        }

        Data.get(nmcontroller + '/index', param).then(function (data) {
            $scope.displayed = data.data.list;
            tableState.pagination.numberOfPages = Math.ceil(data.data.totalItems / limit);
        });

        $scope.isLoading = false;
    };


    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Form Tambah Data";
        $scope.form = {};
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Edit Data : " + form.name;
        $scope.form = form;
        $scope.form.password = "";
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtitle = "Lihat Data : " + form.name;
        $scope.form = form;
    };
    $scope.save = function (form) {

        var url = (form.id > 0) ? nmcontroller + '/update' : nmcontroller + '/create';
        Data.post(url, form).then(function (result) {
          if (result.status_code == 200) {
              $scope.is_edit = false;
              $scope.callServer(tableStateRef);
              toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
            } else {
                  toaster.pop("error","Terjadi Kesalahan",setErrorMessage(result.errors));
            }
        });
    };
    // $scope.generate = function () {
    //     Data.get(nmcontroller + '/list').then(function (result) {
    //         toaster.pop('success', "Menu baru telah terupdate");
    //     });
    // }
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.callServer(tableStateRef);
    };


    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item ini ?")) {
            Data.delete(nmcontroller + '/delete/' + row.id).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };


})
