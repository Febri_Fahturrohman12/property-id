app.controller("profilCtrl", function($scope, Data, toaster, UserService, $location) {
    /**
     * Inialisasi
     */
    $scope.form = {
        password: ""
    };
    $scope.loading = false;
    /** 
     * End inialisasi
     */
    var id = UserService.getUser.id;
    Data.get("appuser/view").then(function(result) {
        $scope.form = result.data;
    });

    $scope.save = function(form) {
        $scope.loading = true;
        var waktu = 5;
        if (typeof form.password == "undefined") {
            Data.post("appuser/save", form).then(function(result) {
                if (result.status_code == 200) {
                    // firebase.auth().currentUser.updateEmail(form.email).then(function() {});
                    $scope.hitungMundur();
                } else {
                    toaster.pop("error","Terjadi Kesalahan",setErrorMessage(result.errors));
                }
                $scope.loading = false;
            });
        } else {
                Data.post("appuser/save", form).then(function(result) {
                    if (result.status_code == 200) {
                        $scope.hitungMundur();
                    } else {
                        toaster.pop("error","Terjadi Kesalahan",setErrorMessage(result.errors));
                    }
                    $scope.loading = false;
                });
            
        }  
    };

    $scope.logout = function() {
        UserService.delUser();
        Data.get("site/logout").then(function(response) {
            $location.path('/login');
            location.reload();
        });
        
    };

    $scope.hitungMundur = function() {
        var waktu = 6;
        document.getElementsByClassName("overlay")[0].style.display = "block";
        setInterval(function() {
            waktu--;
            if (waktu === 0) {
                $scope.logout();
            } else {
                document.getElementById("pesan").innerHTML = waktu;
            }
        }, 1000);
    }
           
            

});
