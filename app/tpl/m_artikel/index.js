app.controller("artikelCtrl", function($scope, Data, toaster, $rootScope) {
    var tableStateRef   = {};
    $scope.displayed    = [];
    $scope.listKeyword  = [];
    $scope.listjudul    = [];
    $scope.listgambar   = [];
    $scope.form         = {};
    $scope.form.detail  = [];
    $scope.gambar       = {};
    $scope.bacajuga     = {};
    $scope.is_create    = false;
    $scope.is_edit      = false;
    $scope.is_view      = false;
    $scope.is_upload    = false;
    $scope.loading      = false;
    $scope.user         = $rootScope.user;
    $scope.opened       = {};
    $scope.preview      = {};
    $scope.karakter     = 0;
    $scope.jmlkata      = 0;

    //Index & Modal Setting
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }

        Data.get("artikel/index", param).then(function(response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });
        $scope.isLoading = false;
    };
    $scope.callServer2 = function callServer(tableState) {
        $scope.isLoading = true;
        tableStateRef = tableState;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 12;
        var param = {
            offset: offset,
            limit: limit
        };

        Data.get("artikel/gambar",param).then(function(response) {
            $scope.gambar = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(response.data.totalItems / limit);
        });

        $scope.isLoading = false;
    };

    //Get Data
    $scope.cariArtikel = function(cari) {
        if (cari.length > 2) {
            Data.get("artikel/judul", {'cari': cari}).then(function(response) {
                $scope.listjudul = response.data.list;
            });
        }
    }
    Data.get('artikel/keywordUtama').then(function (data) {
        $scope.listKeyword = data.data.list;
    });

    //Alias Setting
    $scope.UrlParse = function(value) {
        var awal;
        awal = value.replace(/&nbsp;/gi,"");
        awal = awal.replace(/&amp;/gi,"");
        awal = awal.replace(/&quot;/gi,"");
        awal = awal.replace(/[^a-z0-9 -]/gi, "");
        awal = awal.replace(/ /gi,"-")
        awal = awal.replace(/--/gi, '-');
        awal = awal.replace(/---/gi, '-');
        awal = awal.replace(/----/gi, '-');
        awal = awal.replace(/-----/gi, '-');
        awal = awal.replace(/------/gi, '-');
        awal = awal.replace(/-------/gi, '-');
        $scope.form.alias = awal.toLowerCase();
    };

    //Datepicker Setting   
    $scope.toggle = function ($event, elemId) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened[elemId] = !$scope.opened[elemId];
    };

    //All Keyword Setting
    $scope.select = function(form) {
        $scope.form.kyutm = {
            value: form.keywordUtama.toUpperCase(),
        };
    };
    $scope.kyutma = function(select) {
      $scope.form.keywordUtama = select.url;
    };
    $scope.get_koma = function(value) {
        var awal = value.replace(/[^a-z0-9\n  ,]/gi, '').toLowerCase();
        awal = awal.replace(/\n/gi, ", ").toLowerCase();
        awal = awal.replace(/&nbsp;/gi," ").toLowerCase();
        awal = awal.replace(/,,/gi,", ").toLowerCase();
        awal = awal.replace(/,,,/gi,", ").toLowerCase();
        awal = awal.replace(/,,,,/gi,", ").toLowerCase();
        awal = awal.replace(/,,,,,/gi,", ").toLowerCase();
        $scope.form.keyword = awal.replace(/ ,/gi, ",").toLowerCase();
    };
    
    //Editor Setting
    $scope.create = function(form) {
        var a = new Date();
        $scope.is_edit = true;
        $scope.is_create = true;
        $scope.is_upload = false;
        $scope.is_view = false;
        $scope.form = {};
        $scope.form.tanggal = new Date();
        $scope.form.waktu = new Date(a.getFullYear(),a.getMonth(),a.getDate(),a.getHours(),a.getMinutes());
        $scope.form.detail = [];
        $scope.form.firebase_id = null;
        $scope.pushSectionAwal();
    };
    $scope.update = function(form) {
        $scope.is_edit = true;
        $scope.is_create = false;
        $scope.is_upload = false;
        $scope.is_view = false;
        $scope.form = form;
        $scope.form.tanggal = new Date(""+form.jam+"");
        $scope.form.waktu = new Date(""+form.jam+"");
        $scope.form.keywordUtama = $scope.form.keywordUtama.toLowerCase()
        $scope.select(form);
        $scope.kata();
    };
    $scope.striptags = function(data,index) {
        if (typeof data !== 'undefined') {
            $scope.form.detail[index].content = data.replace(/<[^>]*>/g,"");
            if (index == 0) {
                $scope.UrlParse($scope.form.detail[index].content);
            }
        } else {
            $scope.form.detail[index].content = '';
        }
    };
    $scope.allpetik = function(data){
        if (typeof data !== 'undefined') {
            $scope.form.deskripsi = data.replace('"',"`");
        } else {
            $scope.form.deskripsi = '';
        }
    };
    $scope.kata = function() {
        $scope.karakter = karakter();
        $scope.jmlkata = kata();
    }
    function karakter(){
        var data = [];
        for (var i = 0; i < $scope.form.detail.length; i++) {
            if ($scope.form.detail[i].posisi === "content" || $scope.form.detail[i].posisi === "title" || $scope.form.detail[i].posisi === "quote") {
                data.push($scope.form.detail[i].content.replace(/&nbsp;/gi," ").replace(/<[^>]*>/gi,"").replace(/&#65279;/gi,""));
            }
        }
        var awal = data.join(" ");
        return awal.length;
    }
    function kata(){
        var data = [];
        for (var i = 0; i < $scope.form.detail.length; i++) {
            if ($scope.form.detail[i].posisi === "content" || $scope.form.detail[i].posisi === "title" || $scope.form.detail[i].posisi === "quote") {
                data.push($scope.form.detail[i].content.replace(/&nbsp;/gi," ").replace(/<br\/>/gi,"\n").replace(/<\/p>/gi,"\n").replace(/<[^>]*>/gi,"").replace(/&#65279;/gi,""));
            }
        }
        var awal = data.join(" ");
        awal = awal.replace(/\n/gi," ");
        var kata = awal.split(" ");
        return kata.length - 1;
    }
    $scope.copyAll = function() {
        var data = [];
        for (var i = 0; i < $scope.form.detail.length; i++) {
            if ($scope.form.detail[i].posisi === "content") {
                data.push($scope.form.detail[i].content.replace(/&nbsp;/gi," ").replace(/<br\/>/gi,"\n").replace(/<\/p>/gi,"\n").replace(/<[^>]*>/gi,"").replace(/&#65279;/gi,""));
            } else if ($scope.form.detail[i].posisi === "quote" || $scope.form.detail[i].posisi === "title") {
                data.push($scope.form.detail[i].content+"\n");
            }
        }
        var awal = data.join("");
        var el = document.createElement('textarea');
        el.value = awal;
        el.setAttribute('readonly', '');
        el.style.position = 'absolute';
        el.style.left = '-9999px';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        alert("Document Berhasil Dicopy");
    }

    //Modal Setting
    $scope.addImage = function($index) {
        $scope.is_upload = true;
        $scope.gambar = {};
        $scope.index = $index;
    };
    $scope.backToList = function($index) {
        $scope.is_upload = false;
        $scope.index = $index;
    };
    $scope.addToBorder = function($value,$index) {
        $scope.form.detail[$index].content = $value.value;
        $scope.form.detail[$index].artikel_gambar_id = $value.id;
        $('#modal').modal('hide');
    };
    $scope.view = function(form){
        $scope.isLoading = true;
        Data.post("artikel/parsePublish",form).then(function(data) {
            if (data.status_code == 200) {
                $scope.preview = data.data.list;
                $scope.is_view = true;
            } else {
                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(data.errors));
            }
            $scope.isLoading = false;
        }); 
    };
    $scope.backView = function(form) {
        $scope.is_view = false;
        $scope.preview = {};
    }
    
    //Delete
    $scope.delete = function(row) {
        if (confirm("Apa anda yakin akan MENGHAPUS artikel ini ?")) {
            row.is_deleted = 1;
            Data.post("artikel/delete", row).then(function(result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    //Save Setting
    $scope.save = function(form,status) {
        $scope.loading = true;
        $scope.form.status = status;

        var datas = [];
        var div= document.createElement("div");
        div.className += "overlay";
        document.body.appendChild(div);
        
        if (status == 'publish') {
            if (isNaN(form.firebase_id)) {
                Data.post("artikel/parsePublish",form).then(function(data) {
                    datas = data.data.list;
                    db.collection('artikel').doc(form.firebase_id).set(datas).then(function (docRef) {
                        Data.post("artikel/save", form).then(function(result) {
                            if (result.status_code == 200) {
                                toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                                $scope.cancel();
                            } else {
                                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                            }
                            $scope.loading = false;
                            $(".overlay").remove();
                        });
                    });
                });
            } else {
                Data.post("artikel/parsePublish",form).then(function(data) {
                    datas = data.data.list;
                    db.collection('artikel').add(datas).then(function (docRef) {
                        $scope.form.firebase_id = docRef.id;
                        Data.post("artikel/save", form).then(function(result) {
                            if (result.status_code == 200) {
                                toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                                $scope.cancel();
                            } else {
                                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                            }
                            $scope.loading_upload = false;
                            $(".overlay").remove();
                        });
                    });
                });
            }  
        } else {
            if (isNaN(form.firebase_id)) {
                db.collection("artikel").doc(form.firebase_id).delete().then(querySnapshot => {
                    $scope.form.firebase_id = null;
                    Data.post("artikel/save", form).then(function(result) {
                        if (result.status_code == 200) {
                            toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                            $scope.cancel();
                        } else {
                            toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                        }
                        $scope.loading_upload = false;
                        $(".overlay").remove();
                    });
                });
            } else {
                Data.post("artikel/save", form).then(function(result) {
                    if (result.status_code == 200) {
                        toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                        $scope.cancel();
                    } else {
                        toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                    }
                    $scope.loading_upload = false;
                    $(".overlay").remove();
                });
            }
        }
    };
    $scope.savegambar = function(gambar,$index) {
        $scope.loading_upload = true;
        fetch(gambar.value).then(function(response) {return response.blob();}).then(function(primary) {
            ref.child(gambar.nameprimary).put(primary).then((success) => {
              success.ref.getDownloadURL().then(function(UrlPrimary) {
                $scope.gambar.value = UrlPrimary;
                $scope.gambar.primaryLama = gambar.nameprimary;
                //Thumb
                fetch(gambar.thumb).then(function(response) {return response.blob();}).then(function(thumb) {
                    ref.child(gambar.namethumb).put(thumb).then((success) => {
                      success.ref.getDownloadURL().then(function(UrlThumb) {
                        $scope.gambar.thumb = UrlThumb;
                        $scope.gambar.thumbLama = gambar.namethumb;
                        
                        //Save Data
                        Data.post("artikel/save-image", gambar).then(function(result) {
                            if (result.status_code == 200) {
                                toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                                $scope.backToList($index);
                            } else {
                                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                            }
                        });
                        $scope.loading_upload = false;
                      });
                    });
                });
              });
            });
        });
    };
    $scope.viral = function(form) {
        $scope.loading = true;
        form.is_viral = 1;
        var datas = {
            is_viral : 1,
        }
        
        db.collection('artikel').doc(form.firebase_id).update(datas).then(function (docRef) {
            Data.post("artikel/is_viral", form).then(function(result) {
                if (result.status_code == 200) {
                    toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                    $scope.cancel();
                } else {
                    toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                }
                $scope.loading = false;
                $scope.callServer(tableStateRef);
            });
        });
    };
    $scope.unviral = function(form) {
        $scope.loading = true;
        form.is_viral = 0;
        var datas = {
            is_viral : 0,
        }
        
        db.collection('artikel').doc(form.firebase_id).update(datas).then(function (docRef) {
            Data.post("artikel/is_viral", form).then(function(result) {
                if (result.status_code == 200) {
                    toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                    $scope.cancel();
                } else {
                    toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
                }
                $scope.loading = false;
                $scope.callServer(tableStateRef);
            });
        });
    };
    $scope.draf = function(form) {
        $scope.loading = true;
        form.status = "draf";
        
        Data.post("artikel/draf", form).then(function(result) {
            if (result.status_code == 200) {
                toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
                location.reload()
            } else {
                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
            $scope.loading = false;
        });
    };

    //Gambar setting
    $scope.readURL = function(input) {
        Data.post("artikel/create-image", input).then(function(result) {
            $scope.gambar.value = result.data.path;
            $scope.gambar.nameprimary = result.data.primary;
            $scope.gambar.namethumb = result.data.thumb
            $scope.gambar.thumb = result.data.pathThumb;
        }); 
    };

    //Cancel Setting
    $scope.cancel = function() {
        $scope.is_edit = false;
        $scope.callServer(tableStateRef);
    };

    //Select Setting
    $scope.bcjg = function(form,$index) {
        $key = '';
        angular.forEach($scope.form.detail, function(value, key) {
            if (value.posisi === 'bacajuga') {
                $key = key;
            }
        });

        $scope.form.detail[$key].content[$index].artikel_id = form.id;
        $scope.form.detail[$key].content[$index].judul = form.judul;
        $scope.form.detail[$key].content[$index].alias = form.alias;
    };

    //Add Element Setting
    $scope.pushSectionAwal = function() {
        $scope.form.detail.push({
            content:'',
            posisi:'judul'
        });
    };
    $scope.pushSection = function() {
        $scope.form.detail.push({
            content:'',
            posisi:'title'
        });

        $scope.form.detail.push({
            content:'',
            posisi:'image'
        });

        $scope.form.detail.push({
            content:'',
            posisi:'content'
        });
    };
    $scope.pushHeading = function() {
        $scope.form.detail.push({
            content:'',
            posisi:'title'
        });
    };
    $scope.pushParagraph = function() {
        $scope.form.detail.push({
            content:'',
            posisi:'content'
        });
    };
    $scope.pushGambar = function() {
        $scope.form.detail.push({
            content:'',
            posisi:'image'
        });
    };
    $scope.pushQuote = function() {
        $scope.form.detail.push({
            content:'',
            posisi:'quote'
        });
    };
    $scope.pushBcjg = function() {
        $scope.form.detail.push({
            content: [],
            posisi:'bacajuga'
        });
    };
    $scope.pushContentBcjg = function($key) {
        $scope.form.detail[$key].content.push({
            judul:'',
            alias:'',
            artikel_id:''
        });
    };
    $scope.pushEmbed = function() {
        $scope.form.detail.push({
            content:'',
            posisi:'embedelement'
        });
    };
    $scope.splice = function (paramindex) {
        if (confirm("Apa anda yakin akan MENGHAPUS element ini ?")) {
            if (isNaN($scope.form.detail[paramindex].id)) {
                $scope.form.detail.splice(paramindex, 1);
                toaster.pop("success", "Berhasil", "Element Terhapus");
            } else {
                Data.post("artikel/delete_detail", $scope.form.detail[paramindex]).then(function(result) {
                    $scope.form.detail.splice(paramindex, 1);
                    toaster.pop("success", "Berhasil", "Element Terhapus");
                });
            }
            
        }
    };
    $scope.spliceContentBcjg = function (paramindex) {
        if (confirm("Apa anda yakin akan MENGHAPUS element ini ?")) {
            $key = '';
            angular.forEach($scope.form.detail, function(value, key) {
                if (value.posisi === 'bacajuga') {
                    $key = key;
                }
            });

            $scope.form.detail[$key].content.splice(paramindex, 1);
            toaster.pop("success", "Berhasil", "Element Terhapus");
        }
    };
});