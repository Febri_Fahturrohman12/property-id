app.controller('kategoriGaleriCtrl', function ($scope, Data, toaster) {

    //init data
    var tableStateRef;
    var nmcontroller = "app_galeri_kategori";

    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;


    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {offset: offset, limit: limit};

        if (tableState.sort.predicate) {
            param['sort'] = tableState.sort.predicate;
            param['order'] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param['filter'] = tableState.search.predicateObject;
        }

        Data.get(nmcontroller + '/index', param).then(function (data) {
            $scope.displayed = data.data.list;
            tableState.pagination.numberOfPages = Math.ceil(data.data.totalItems / limit);
        });

        $scope.isLoading = false;
    };


    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_update = false;
        $scope.formtitle = "Form Tambah Data";
        $scope.form = {};
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_update = true;
        $scope.formtitle = "Edit Data : " + form.nama;
        $scope.form = form;
        $scope.form.password = "";
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.is_update = false;
        $scope.formtitle = "Lihat Data : " + form.nama;
        $scope.form = form;
    };

    $scope.hapus_gambar = function (param){
      Data.post(nmcontroller + '/hapusGambar', param).then(function(result) {
        toaster.pop("success", "Berhasil", "Gambar berhasil dihapus");
        $scope.form.img = undefined;
      });
    }

    $scope.save = function (form) {
        var url = (form.id > 0) ? nmcontroller + '/update' : nmcontroller + '/create';
        Data.post(url, form).then(function (result) {
            if (result.status_code == 200) {
                $scope.is_edit = false;
                $scope.callServer(tableStateRef);
                toaster.pop("success", "Berhasil", "Data berhasil tersimpan");
            } else {
                  toaster.pop("error","Terjadi Kesalahan",setErrorMessage(result.errors));
            }
        });
    };
    // $scope.generate = function () {
    //     Data.get(nmcontroller + '/list').then(function (result) {
    //         toaster.pop('success', "Menu baru telah terupdate");
    //     });
    // }
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.callServer(tableStateRef);
    };


    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item ini ?")) {
            Data.delete(nmcontroller + '/delete/' + row.id).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };

    $scope.striptags = function(data) {
        console.log(data);
        if (typeof data !== 'undefined') {
            $scope.form.alias = data.replace(/<[^>]*>/g,"");
            if ($scope.form.alias) {
                $scope.UrlParse($scope.form.alias);
            }
        } else {
            $scope.form.alias = '';
        }
    };
    $scope.UrlParse = function(value) {
        var awal;
        awal = value.replace(/&nbsp;/gi,"");
        awal = awal.replace(/&amp;/gi,"");
        awal = awal.replace(/&quot;/gi,"");
        awal = awal.replace(/[^a-z0-9 -]/gi, "");
        awal = awal.replace(/ /gi,"-")
        awal = awal.replace(/--/gi, '-');
        awal = awal.replace(/---/gi, '-');
        awal = awal.replace(/----/gi, '-');
        awal = awal.replace(/-----/gi, '-');
        awal = awal.replace(/------/gi, '-');
        awal = awal.replace(/-------/gi, '-');
        $scope.form.alias = awal.toLowerCase();
    };

})
