angular.module('app').controller('dashboardCtrl', function($scope, Data, $state, UserService, $location) {
	var user = UserService.getUser();
	
	if (user === null) {
        $location.path('/login');        
    } else {
    	var date = new Date();
    	$scope.artikel = {};
    	$scope.artikel.bulanini = {};
    	$scope.artikel.bulanlalu = {};
    	$scope.artikel.selamaini = {};

    	//Setting bulan
		var arrbulan = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    	$scope.bulanini = arrbulan[date.getMonth()];
    	if (date.getMonth() === 0) {
    		$scope.bulanlalu = arrbulan[11];
    	} else {
    		$scope.bulanlalu = arrbulan[date.getMonth() - 1];
    	}

    	//Get Hari ini
    	Data.get("dashboard/atikel_hari_ini",user).then(function(response) {
            $scope.artikel.hariini = response.data.list;
        });

    	//Get Bulan ini
        Data.get("dashboard/atikel_bulan_ini",user).then(function(response) {
            $scope.artikel.bulanini.artikel = response.data.artikel;
            $scope.artikel.bulanini.viral 	= response.data.viral;
            $scope.artikel.bulanini.total 	= response.data.total;
            $scope.artikel.bulanini.totalHarga 	= response.data.totalHarga;
        });

        Data.get("dashboard/atikel_bulan_lalu",user).then(function(response) {
            $scope.artikel.bulanlalu.artikel 	= response.data.artikel;
            $scope.artikel.bulanlalu.viral 		= response.data.viral;
            $scope.artikel.bulanlalu.total 		= response.data.total;
            $scope.artikel.bulanlalu.totalHarga = response.data.totalHarga;
        });

        Data.get("dashboard/atikel_selama_ini",user).then(function(response) {
            $scope.artikel.selamaini.artikel 	= response.data.artikel;
            $scope.artikel.selamaini.viral 		= response.data.viral;
            $scope.artikel.selamaini.total 		= response.data.total;
            $scope.artikel.selamaini.totalHarga = response.data.totalHarga;
        });

        Data.get("dashboard/artikel",user).then(function(response) {
            $scope.displayedartikel    = response.data.list;
        });

        Data.get("dashboard/testimoni",user).then(function(response) {
            $scope.displayed    = response.data.list;
        });

        $scope.approve = function (row) {
            if (confirm("Apa anda yakin akan Approve item ini ?")) {
                row.publish = 1;
                Data.post("dashboard/is_approve", row).then(function(result) {
                    $scope.callServer(tableStateRef);
                });
            }
        };

        $scope.reject = function (row) {
            if (confirm("Apa anda yakin akan Reject item ini ?")) {
                row.publish = 0;
                Data.post("dashboard/is_approve", row).then(function(result) {
                    $scope.callServer(tableStateRef);
                });
            }
        };
    }

});