app.controller("masterMenuCtrl", function($scope, Data, toaster) {
    /**
     * Inialisasi
     */
    $scope.form = {};
    $scope.form.hot = [];
    $scope.form.artikel = [];
    $scope.form.minggu_ini = [];
    $scope.listArtikel = [];
    $scope.loading = false;
    /**
     * End inialisasi
     */
     
    //Get Data
    Data.get("m_menu/index").then(function(response) {
        $scope.form.hot = response.data.hot;
        $scope.form.artikel = response.data.minggu_ini;
        $scope.select($scope.form.artikel);
    });
    $scope.cariArtikel = function(cari) {
        if (cari.length > 2) {
            Data.get("m_menu/artikel",{'cari': cari}).then(function(response) {
                $scope.listArtikel = response.data.list;
            });
        }
    }
    Data.get("m_menu/artikel").then(function(response) {
        $scope.listArtikel = response.data.list;
    });

    //Select Data
    $scope.select = function(artikel){
        for (var i = 0; i < artikel.length; i++) {
            $scope.form.minggu_ini[i] = artikel[i];
        }
    };
    $scope.artkl = function(form,$index,artikellama){
        $scope.artikel[$index] = {
            artikel_id : form.id,
            judul: form.judul,
            alias: form.alias,
            posisi: $index + 1,
            gambar_thumb:form.gambar
        };
    };

    //Hapus
    $scope.trash = function(row) {
        if (confirm("Apa anda yakin akan MENGHAPUS item ini ?")) {
            $scope.form.minggu_ini.splice($scope.form.minggu_ini.indexOf(row), 1);
        }
    };

    //Simpan
    $scope.save = function(form) {
        $scope.loading = true;
        Data.post("m_menu/save", form).then(function(result) {
            if (result.status_code == 200) {
                db.collection('menuCon').doc("uI7agnZcTr").set(result.data).then(function (docRef) {
                    location.reload();
                }); 
            } else {
                toaster.pop("error", "Terjadi Kesalahan", setErrorMessage(result.errors));
            }
            $scope.loading = false;
        });
    };
});