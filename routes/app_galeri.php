<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        'judul'       => 'required',
    );

    // GUMP::set_field_name("nama", "Nama Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get('/app_galeri/getKategori', function ($request, $response) {
    $db = $this->db;

    $data = $db->findAll('select * from galeri_kategori');

    return successResponse($response, $data);
});


/**
 * get user list
 */
$app->get('/app_galeri/index', function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("galeri.*, galeri_kategori.nama as nama_kategori")
        ->from('galeri')
        ->join('left join', 'galeri_kategori', 'galeri.galeri_kategori_id = galeri_kategori.id')
        ->orderBy('galeri.id desc');

    /** set parameter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'judul') {
                $db->where('galeri.judul', 'LIKE', $val);
            } else if ($key == 'kategori_id') {
                $db->where('galeri.galeri_kategori_id', '=', $val);
            }else {
                $db->where($key, 'LIKE', $val);
            }
        }
    }

    /** Set limit */
    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    /** Set offset */
    if (isset($params['offset']) && !empty($params['offset'])) {
        $db->offset($params['offset']);
    }

    /** Set sorting */
    if (isset($params['sort']) && !empty($params['sort'])) {
        $db->orderBy($params['sort']);
    }

    $models    = $db->findAll();
    $totalItem = $db->count();
    // print_r($models);exit;

    foreach ($models as $key => $value) {
        $models[$key] = (array)$value;
        $models[$key]['galeri_kategori_id']    = $db->select('*')->from('galeri_kategori')->where('id', '=', $value->galeri_kategori_id)->find();
    }


    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * create user
 */
$app->post('/app_galeri/create', function ($request, $response) {
    $db   = $this->db;
    $data = json_decode(file_get_contents("php://input"), true);

    if (!empty($data['file_foto']['base64'])) {
            $folder = 'app/img/gambar_galeri/';
            if (!is_dir($folder)) {
                mkdir($folder, 0777);
            }
            $simpan_foto = base64ToFile($data['file_foto'], $folder);
            $temp        = explode(".", $simpan_foto["fileName"]);
            $newfilename = 'Image' .  $data['judul']. '-' . random(4) . '.' . end($temp);
            rename($simpan_foto['filePath'], $folder . $newfilename);

            $data['img'] = $newfilename;
        }


    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $data['galeri_kategori_id'] = $data['galeri_kategori_id']['id'];
            $model = $db->insert("galeri", $data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * update user
 */
$app->post('/app_galeri/update', function ($request, $response) {
    $data = json_decode(file_get_contents("php://input"), true);
    $db = $this->db;

    if (empty($data['file_foto']['base64'])) {
              unset($data['img']);
    } else {
        $folder = 'app/img/gambar_galeri/';
        //hapus foto lama
        $fotoLama = $db->select("img")->from("galeri")
            ->where("id", "=", $data['id'])->find();

        if (!empty($fotoLama->img)) {
            unlink($folder . $fotoLama->img);
        }

        //upload file foto yg baru
        if (!is_dir($folder)) {
            mkdir($folder, 0777);
        }

        $simpan_foto = base64ToFile($data['file_foto'], $folder);
        $temp        = explode(".", $simpan_foto["fileName"]);
        $newfilename = 'Image' .  $data['judul']. '-' . randomImage(4) . '.' . end($temp);
        rename($simpan_foto['filePath'], $folder . $newfilename);

        $data['img'] = $newfilename;
    }

    $validasi = validasi($data);
    if ($validasi === true) {
        try {
          $data['galeri_kategori_id'] = $data['galeri_kategori_id']['id'];
          $model = $db->update("galeri", $data, array('id' => $data['id']));
          return successResponse($response, $model);

        } catch (Exception $e) {
          return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post('/app_galeri/hapusGambar', function ($request, $response) {
      $data = json_decode(file_get_contents("php://input"), true);
      $db = $this->db;
      $folder = 'app/img/gambar_galeri/';
      // print_r($data['id']);exit();
      $fotoLama = $db->select("img")->from("galeri")
      ->where("id", "=", $data['id'])->find();


      if (!empty($fotoLama->img)) {
        unlink($folder . $fotoLama->img);
      }

      $db->update('galeri', ['img' => ""], ['id' => $data['id']]);

      return successResponse($response, ['Berhasil Menghapus']);

});

$app->post('/app_galeri/trash', function ($request, $response) {

    $data = json_decode(file_get_contents("php://input"), true);
    // print_r($data);exit();
    $db                  = $this->db;
    // $data['tgl_nonaktif'] = date('Y-m-d', strtotime($data['tgl_nonaktif']));
    // $validasi            = validasi($data);
    $datas['is_deleted'] = $data['is_deleted'];
    // $datas['tgl_nonaktif'] = date('Y-m-d');
        try {
            $model = $db->update("galeri", $datas, array('id' => $data['id']));
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
});

/**
 * delete user
 */
$app->delete('/app_galeri/delete/{id}', function ($request, $response) {
    $sql = $this->db;
    $folder = 'app/img/gambar_galeri/';
      // print_r($data['id']);exit();
      $fotoLama = $sql->select("img")->from("galeri")
      ->where("id", "=", $request->getAttribute('id'))->find();

      if (!empty($fotoLama->img)) {
        unlink($folder . $fotoLama->img);
      }
    $delete = $sql->delete('galeri', array('id' => $request->getAttribute('id')));
    echo json_encode(array('status' => 1));
});
