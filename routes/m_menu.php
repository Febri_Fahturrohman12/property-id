<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 
 /**
 * Ambil semua list menu
 */
$app->get("/m_menu/index", function ($request, $response) {
    $db = $this->db;
    $getFile = file_get_contents(__DIR__."/json/menuCon.json");
    $data = json_decode($getFile);
    return successResponse($response, ["hot" => $data->hot,"minggu_ini" => $data->minggu_ini]);
});
$app->get("/m_menu/artikel", function ($request, $response) {
    $db     = $this->db;
    $params = $request->getParams();

    $models    = $db->findAll("SELECT artikel.* FROM artikel where is_viral = 1 AND judul LIKE '%".$params['cari']."%' ORDER BY jam DESC");
    foreach ($models as $val) {
        $gambar         = $db->findAll("SELECT * FROM `artikel_detail` WHERE posisi = 'image' AND artikel_id =".$val->id);
        if (empty($gambar)) {
            $val->gambar = "";
        } else {
            $val->gambar = $gambar[0]->content;
        }
    }
    return successResponse($response, ["list" => $models]);
});
/**
 * save menu
 */
$app->post("/m_menu/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;

    try {
        foreach ($data['hot'] as $key => $value) {
            if (!empty($data['hot'][$key]['value'])) {
                $hot[$key]['value'] = strtoupper($data['hot'][$key]['value']);
                $hot[$key]['url'] = strtolower(str_replace(" ", "-", $data['hot'][$key]['value']));
                if (isset($data['hot'][$key]['is_hot'])) {
                    $hot[$key]['is_hot'] = $data['hot'][$key]['is_hot'];
                } else {
                    $hot[$key]['is_hot'] = 'false';
                }
            }
        }

        foreach ($data['minggu_ini'] as $key => $value) {
            if (!empty($data['minggu_ini'][$key])) {
                $minggu_ini[$key] = (array)$value;
                $minggu_ini[$key]['alias'] = date("Y/m/",$minggu_ini[$key]["jam"]).$minggu_ini[$key]["alias"];
            }
        }
        $parse['hot'] = $hot;
        $parse['minggu_ini'] = $minggu_ini;
        file_put_contents(__DIR__."/json/menuCon.json",json_encode($parse));
        return successResponse($response, $parse);   
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
});
