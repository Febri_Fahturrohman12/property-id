<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "judul" => "required",
        "alias" => "required",
        "deskripsi" => "required",
        // "keywordUtama" => "required",
        "kategori" => "required",
        // "keyword" => "required",
        "status" => "required",
        "waktu" => "required",
        "tanggal" => "required"
        // "gambarPrimary" => "required"
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

//Index
$app->get("/artikel/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("artikel.*, m_user.nama as penulis, artikel_kategori.nama as nama_kategori, artikel_kategori.id as id_kategori")
        ->from("artikel")
        ->join("LEFT JOIN","m_user","artikel.created_by = m_user.id")
        ->join("LEFT JOIN","artikel_kategori","artikel.kategori = artikel_kategori.id")
        ->orderBy("artikel.jam DESC");

    // if ($_SESSION['user']['status'] == 'Penulis') {
    //     $db->where("created_by","=",$_SESSION['user']['id']);
    // } else {
    //     $db->where("status","!=","draf");
    // }
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            if ($key == 'status') {
            $db->where('artikel.status', '=', $val);
          } elseif ($key == 'kategori') {
            $db->andWhere('artikel.kategori', 'LIKE', $val);
          } elseif ($key == 'judul') {
            $db->andWhere('artikel.judul', 'LIKE', $val);
          }
        }
    }

    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }


    $models    = $db->findAll();
    
    $totalItem = $db->count();
    foreach ($models as $val) {
        $detail         = $db->findAll("SELECT * FROM artikel_detail WHERE artikel_id =".$val->id." ORDER BY id ASC");

        $gambar         = $db->findAll("SELECT artikel_detail.*,artikel_gambar.thumb FROM `artikel_detail`,`artikel_gambar` WHERE artikel_detail.artikel_gambar_id = artikel_gambar.id AND posisi = 'image' AND artikel_id =".$val->id);

        $val->url       = config('SITE_DESKTOP').date('Y/m/',$val->jam).$val->alias;

        $val->tanggal   = date('d/m/Y',$val->jam);
        $val->waktu     = date("H:i",$val->jam);
        $val->jam       = date("Y-m-d H:i",$val->jam);
        $val->kategori  = ["id"=>$val->id_kategori, "nama"=>$val->nama_kategori];


        $keywordA = explode(", ", $val->keyword);
        $keywordB = [];
        $val->keywordUtama = $keywordA[0];
        for ($i=1; $i < count($keywordA); $i++) { 
            $keywordB[] = $keywordA[$i];
        }
        $val->keyword = implode(", ", $keywordB);

        if (empty($gambar)) {
            $val->gambar = "";
            $val->gambar_thumb = "";
        } else {
            $val->gambar = $gambar[0]->content;
            $val->gambar_thumb = $gambar[0]->thumb;
        }
       
        $val->detail    = $detail;

        foreach ($val->detail as $vall) {
            if ($vall->posisi == "bacajuga") {
                $vall->content = $db->findAll("SELECT artikel_bacajuga.id,artikel.judul,artikel.alias,artikel_bacajuga.artikel_id FROM artikel , artikel_bacajuga WHERE artikel.id = artikel_bacajuga.artikel_id AND artikel_detail_id =".$vall->id." ORDER BY id ASC");
            }
        }
        
    }

    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});

//List Item
$app->get("/artikel/keywordUtama", function ($request, $response) {
    // $getFile = file_get_contents(__DIR__."/json/keywordUtama.json");
    $db = $this->db;
    $db->select("*")
        ->from("artikel_kategori")
        ->orderBy("id DESC");
    // $data = json_decode($getFile);
    $data = $db->findAll();
    return successResponse($response, ["list" => $data]);
});
$app->get("/artikel/gambar", function ($request, $response) {
    $db     = $this->db;
    $params = $request->getParams();

    $db->select("*")
        ->from("artikel_gambar")
        ->where("created_by","=", $_SESSION['user']['id'])
        ->orderBy("id DESC");
    /**
     * Set limit dan offset
     */
   
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }

    $totalItem = $db->count();
    $models    = $db->findAll();
   
   
    return successResponse($response, ["list" => $models,"totalItems"=> $totalItem]);
});
$app->get("/artikel/judul", function ($request, $response) {
    $db     = $this->db;
    $params = $request->getParams();
    $models    = $db->findAll("SELECT * FROM artikel WHERE status = 'publish' AND judul LIKE '%".$params['cari']."%' ORDER BY jam DESC");
    return successResponse($response, ["list" => $models]);
});

//Save
$app->post("/artikel/save", function ($request, $response) {
    $data     = $request->getParams();
    // print_r($data);die();
    $db       = $this->db;
    //judul
    if (isset($data['detail'][0]['content'])) {
        $data['judul']  = $data['detail'][0]['content'];
    }
    
    //Get image
    for ($i=0; $i < count($data['detail']); $i++) { 
        if ($data['detail'][$i]['posisi'] == "image") {
            $img[] = $data['detail'][$i]['content'];
        }
    }
    if (isset($img)) {
        $data['gambarPrimary'] = $img[0];
    }
    
        
    $validasi = validasi($data);
    if ($validasi === true) {
        //Tanggal
        $harijam        =  date('Y-m-d', strtotime($data['tanggal'])) . ' ' . date('H:i:00', strtotime($data['waktu']));
        $data['jam']    = strtotime($harijam);

        //keyword
        // $data['keyword'] = $data['keywordUtama'].', '.$data['keyword'];
        // $data['keyword'] = $data['keyword'];
        //kategori
        $data['kategori'] = $data['kategori']['id'];
        try {
            if (isset($data["id"])) {
                $model = $db->update("artikel", $data, ["id" => $data["id"]]);
                if (isset($data['detail'])) {
                    $array = $data['detail'];
                    foreach ($data['detail'] as $key => $value) {
                        $data['detail'][$key]['artikel_id'] = $data['id'];
                        if (!empty($data['detail'][$key]['content'])) {
                            if (isset($data['detail'][$key]['id'])) {
                                if ($data['detail'][$key]['posisi'] == 'bacajuga') {
                                    foreach ($data['detail'][$key]['content'] as $ky => $val) {
                                        $data['detail'][$key]['content'][$ky]['artikel_detail_id'] = $data['detail'][$key]['id'];
                                        if (isset($data['detail'][$key]['content'][$ky]['id'])) {
                                            $detailContent = $db->update("artikel_bacajuga",$data['detail'][$key]['content'][$ky], ["id" => $data['detail'][$key]['content'][$ky]['id']]);
                                        } else {
                                            $detailContent = $db->insert("artikel_bacajuga",$data['detail'][$key]['content'][$ky]);
                                        }
                                    }
                                    $data['detail'][$key]['content'] = '';
                                }
                                $detail = $db->update("artikel_detail",$data['detail'][$key], ["id" => $data['detail'][$key]['id']]);
                            } else {
                                if ($data['detail'][$key]['posisi'] == 'bacajuga') {
                                    $data['detail'][$key]['content'] = '';
                                }

                                $detail = $db->insert("artikel_detail",$data['detail'][$key]);
                                if ($array[$key]['posisi'] == 'bacajuga') {
                                    foreach ($array[$key]['content'] as $ky => $val) {
                                        $array[$key]['content'][$ky]['artikel_detail_id'] = $detail->id;
                                        $detailContent = $db->insert("artikel_bacajuga",$array[$key]['content'][$ky]);
                                    }
                                }  
                            }
                        }
                    }
                }
            } else {
                $array = $data['detail'];
                $model = $db->insert("artikel", $data);
                foreach ($data['detail'] as $key => $value) {
                    if (!empty($data['detail'][$key]['content'])) {
                        $data['detail'][$key]['artikel_id'] = $model->id;
                        if ($data['detail'][$key]['posisi'] == 'bacajuga') {
                            $data['detail'][$key]['content'] = '';
                        }
                        $detail = $db->insert("artikel_detail",$data['detail'][$key]);
                        if ($array[$key]['posisi'] == 'bacajuga') {
                            foreach ($array[$key]['content'] as $ky => $val) {
                                $array[$key]['content'][$ky]['artikel_detail_id'] = $detail->id;
                                $detailContent = $db->insert("artikel_bacajuga",$array[$key]['content'][$ky]);
                            }
                        }        
                    }        
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }

    return unprocessResponse($response, $validasi);
});
$app->post("/artikel/save-image", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    try {
        $folder = 'app/img/artikel';
        if (!is_dir($folder)) {
            mkdir($folder, 0777);
        }

        if (isset($data["id"])) {
            $model = $db->update("artikel_gambar", $data, ["id" => $data["id"]]);
        } else {
            $model = $db->insert("artikel_gambar", $data);
            // unlink($folder.$data['primaryLama']);
            // unlink($folder.$data['thumbLama']);
        }
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    
    return unprocessResponse($response, $validasi);
});
$app->post("/artikel/is_viral", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
     
    try {
        $model = $db->update("artikel", ["is_viral" => $data['is_viral']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});
$app->post("/artikel/draf", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
     
    try {
        $model = $db->update("artikel", ["status" => $data['status']], ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});
$app->post("/artikel/viral_minggu_ini", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
     try {
        $no = 1;
        foreach ($data as $key => $value) {
            if (isset($data[$key]["id"])) {
                $model = $db->update("m_viral_minggu_ini", $data[$key], ["id" => $data[$key]["id"]]);
            } else {
                $model = $db->insert("m_viral_minggu_ini", $data[$key]);
            }
        }
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
});

//Create And Resize Image
$app->post('/artikel/create-image', function ($request, $response) {
    // $data = $request->getParams();
    $data = json_decode(file_get_contents("php://input"), true);
    // print_r($data);die();
    try {
        // if (!empty($data['value']['base64'])) {
        //     $folder = 'app/img/artikel/';
        //     if (!is_dir($folder)) {
        //         mkdir($folder, 0777);
        //     }
        //     $simpan_foto = base64ToFile($data['value'], $folder);
        //     // print_r($simpan_foto);die();
        //     $temp        = explode(".", $simpan_foto["fileName"]);
        //     $newfilename = 'Image-' . random(4) . '.' . end($temp);
        //     rename($simpan_foto['filePath'], $folder . $newfilename);

        //     $data['img'] = $newfilename;
        // }

        $folder = 'app/img/artikel/';
        if (!is_dir($folder)) {
            mkdir($folder, 0777);   
        }

        $simpan_foto    = base64ToFile($data['value'], $folder);
        // $image          = createImage($folder, $simpan_foto['fileName'], '', true);
        // $primaryImage   = 'artikel-'.date('is').'-'.$image['big'];
        // $thumbImage     = 'thumb-'.date('is').'-'.$image['thumb'];

        // rename($folder.$image['big'], $folder.$primaryImage);
        // rename($folder.$image['thumb'], $folder.$thumbImage);
        //unlink($folder.);

        return successResponse($response, ["path" => config('SITE_IMG').'/artikel/'.$simpan_foto['fileName'], "primary" => $simpan_foto['fileName'],"pathThumb" => config('SITE_IMG').'/artikel/'.$simpan_foto['fileName'], "thumb" => $simpan_foto['fileName']]);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);  
});

//Parse
$app->post('/artikel/parsePublish', function ($request, $response) {
    $data  = $request->getParams();
    $parse = [];
    $db    = $this->db;
    //Status
    if (empty($data['status']) || empty($data['created_at'] || empty($data['penulis']) || empty($data['is_viral']))) {
        $data['status'] = "view";
        $data['created_at'] = strtotime(date("Y-m-d H:i:s"));
        $data['penulis'] = "Penulis Wajibbaca";
        $data['is_viral'] = 0;
    }
    //judul
    if (isset($data['detail'][0]['content'])) {
        $data['judul']  = $data['detail'][0]['content'];
    }
    
    //Get image
    for ($i=0; $i < count($data['detail']); $i++) { 
        if ($data['detail'][$i]['posisi'] == "image") {
            $img[] = $data['detail'][$i]['content'];
        }
    }
    if (isset($img)) {
        $data['gambarPrimary'] = $img[0];
        if (empty($data['gambar']) || empty($data['gambar_thumb'])) {
            $data['gambar'] = $img[0];
            $data['gambar_thumb'] = $img[0];
        }
    }
   
    $validasi = validasi($data);
    if ($validasi === true) {
         //Parse
        $parse['judul']             = $data['detail'][0]['content'];
        $judul                      = str_replace("&amp;", "", str_replace("&nbsp;", "", $data['detail'][0]['content']));

        //Alias
        $parse['alias']             = date("Y/m/",strtotime($data['tanggal'])).$data['alias'];
        
        $parse["created_at"]        = $data["created_at"];
        $parse["created_name"]      = $data['penulis'];
        $parse["description"]       = $data['deskripsi'];
        $parse["gambar_primary"]    = $data["gambar"];
        $parse["gambar_thumb"]      = $data["gambar_thumb"];
        $parse["is_viral"]          = (int)$data["is_viral"];
        $harijam                    =  date('Y-m-d', strtotime($data['tanggal'])) . ' ' . date('H:i:s', strtotime($data['waktu']));
        $parse['jam']               = strtotime($harijam)+25200;
        $parse['keyword']           = explode(", ", $data['keywordUtama'].', '.$data['keyword']);

        //Content
        $content = [];
        $list = [];
        if (isset($data['detail'])) {
            for ($i=1; $i < count($data['detail']); $i++) { 
                if ($data['detail'][$i]['posisi'] == "image") {
                    if ($data['detail'][$i]['content'] != "") {
                        $image = $db->find("SELECT * FROM artikel_gambar WHERE id =".$data['detail'][$i]['artikel_gambar_id']);
                        $data['detail'][$i]['content'] = '<p><img src="'.$data['detail'][$i]['content'].'" alt="'.$parse['judul'].'" /></p><p class="alamat-url">Image from <a href="'.$image->alamat_url.'" target="_blank">'.$image->sumber.'</a></p>';
                    }
                } elseif ($data['detail'][$i]['posisi'] == "title") {
                    $data['detail'][$i]['content'] = "<h2>".$data['detail'][$i]['content']."</h2>";
                } elseif ($data['detail'][$i]['posisi'] == "embedelement") {
                    $data['detail'][$i]['content'] = "<div align='center' class='iframe-full'>".$data['detail'][$i]['content']."</div>";
                } elseif ($data['detail'][$i]['posisi'] == "quote") {
                    $data['detail'][$i]['content'] = "<blockquote class='quote'>".$data['detail'][$i]['content']."</blockquote>";
                } elseif ($data['detail'][$i]['posisi'] == 'bacajuga') {
                    for ($a=0; $a < count($data['detail'][$i]['content']) ; $a++) { 
                        $data['detail'][$i]['content'][$a]['content'] = "<li><a href='".config('SITE_DESKTOP').$data['detail'][$i]['content'][$a]['alias']."'>".$data['detail'][$i]['content'][$a]['judul']."</a></li>";
                       
                        $list[] = $data['detail'][$i]['content'][$a]['content'];
                    }
                    $li = implode("", $list);
                    $data['detail'][$i]['content'] = "<div class='bacajuga'><p><b>Baca Juga:</b></p><ul>".$li."</ul></div>";
                }
                
                $content[] = $data['detail'][$i]['content'];
            }
        }
        
        $parse['content'] = implode("", $content);
        return successResponse($response, ["list" => $parse]);
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Delete
 */
$app->post("/artikel/delete", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
   
    try {
        $db->delete("artikel_detail", ["artikel_id" => $data["id"]]);
        $model = $db->delete("artikel", ["id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
    
    return unprocessResponse($response, $validasi);
});
$app->post("/artikel/delete_detail", function ($request, $response) {
    $data = $request->getParams();
    $db   = $this->db;
   
    try {
        if ($data['posisi'] == "bacajuga") {
            $db->delete("artikel_bacajuga", ["artikel_detail_id" => $data["id"]]);
        }
        $model = $db->delete("artikel_detail", ["id" => $data["id"]]);

        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
    
    return unprocessResponse($response, $validasi);
});

