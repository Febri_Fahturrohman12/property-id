<?php
/**
 * Home
 */
$app->get('/', function ($request, $response) {
    $db     = $this->db;
    return $this->view->render($response, 'content/home.twig', [
        'menu'              => "home",
        'titletab'          => "PROPERTY ID - Boost Your Sales Performance",  
        'sosmed'            => getSettingweb(),        
    ]);
});
