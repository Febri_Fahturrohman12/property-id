<?php
$app->get("/dashboard/atikel_hari_ini", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $hariini 	= strtotime(date("Y-m-d 00:00:00"));
    $besok		= strtotime("+1 day", $hariini);

    $db->select("count(*) as jumlah")->from("artikel");
    if ($params['status'] == 'Penulis') {
    	$db->where("created_at",">=",$hariini)->where("created_at","<=",$besok)->where("created_by","=",$params['id']);
    } else {
    	$db->where("modified_at",">=",$hariini)->where("modified_at","<=",$besok)->where("modified_by","=",$params['id']);
    }
  	
    $models = $db->find();
    return successResponse($response, ["list" => $models->jumlah]);
});

$app->get("/dashboard/atikel_bulan_ini", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $MountAwal 	= strtotime(date("Y-m-01 00:00:00"));
    $MountAkhir	= strtotime("+1 month", $MountAwal);
    if ($params['status'] == "Penulis") {
    	$At = "created_at";
    	$By = "created_by";
    } else {
    	$At = "modified_at";
    	$By = "modified_by";
    }

    //Artikel
    $artikel = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE ".$By."=".$params['id']." AND is_viral = 0 AND ".$At." >= ".$MountAwal." AND ".$At." <= ".$MountAkhir);

    $viral  = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE ".$By."=".$params['id']." AND is_viral = 1 AND ".$At." >= ".$MountAwal." AND ".$At." <= ".$MountAkhir);

   	$total = ($artikel->jumlah) + ($viral->jumlah);
   	$totalHarga = ($artikel->jumlah*2500) + ($viral->jumlah*10000);

    return successResponse($response, [
    	"artikel" 	=> $artikel->jumlah,
    	"viral" 	=> $viral->jumlah,
    	"total" 	=> $total,
    	"totalHarga"=> $totalHarga
    ]);
});

$app->get("/dashboard/atikel_bulan_lalu", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    $MountAwal 	= strtotime(date("Y-m-01 00:00:00"));
    $MountAkhir	= strtotime("-1 month", $MountAwal);
    if ($params['status'] == "Penulis") {
    	$At = "created_at";
    	$By = "created_by";
    } else {
    	$At = "modified_at";
    	$By = "modified_by";
    }

    //Artikel
    $artikel = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE ".$By."=".$params['id']." AND is_viral = 0 AND ".$At." >= ".$MountAkhir." AND ".$At." <= ".$MountAwal);

    $viral = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE ".$By."=".$params['id']." AND is_viral = 1 AND ".$At." >= ".$MountAkhir." AND ".$At." <= ".$MountAwal);


   	$total = ($artikel->jumlah) + ($viral->jumlah);
   	$totalHarga = ($artikel->jumlah*2500) + ($viral->jumlah*10000);
    return successResponse($response, [
    	"artikel" 	=> $artikel->jumlah,
    	"viral" 	=> $viral->jumlah,
    	"total" 	=> $total,
    	"totalHarga"=> $totalHarga
    ]);
});

$app->get("/dashboard/atikel_selama_ini", function ($request, $response) {
    $db = $this->db;
    $params = $request->getParams();

    if ($params['status'] == "Penulis") {
    	$By = "created_by";
    } else {
    	$By = "modified_by";
    }

    //Artikel
    $artikel = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE ".$By."=".$params['id']." AND is_viral = 0");

    $viral = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE ".$By."=".$params['id']." AND is_viral = 1");

   	$total = ($artikel->jumlah) + ($viral->jumlah);
   	$totalHarga = ($artikel->jumlah*2500) + ($viral->jumlah*10000);

    return successResponse($response, [
    	"artikel" 	=> $artikel->jumlah,
    	"viral" 	=> $viral->jumlah,
    	"total" 	=> $total,
    	"totalHarga"=> $totalHarga
    ]);
});


$app->get('/dashboard/testimoni', function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("*")
        ->from('testimoni')
        ->limit(4)
        ->orderBy('testimoni.id desc');

    /** Set limit */
    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    /** Set offset */
    if (isset($params['offset']) && !empty($params['offset'])) {
        $db->offset($params['offset']);
    }

    /** Set sorting */
    if (isset($params['sort']) && !empty($params['sort'])) {
        $db->orderBy($params['sort']);
    }

    $models    = $db->findAll();
    $totalItem = $db->count();


    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

$app->post('/dashboard/is_approve', function ($request, $response) {

    $data = json_decode(file_get_contents("php://input"), true);
    // print_r($data);exit();
    $db                  = $this->db;
    // $data['tgl_nonaktif'] = date('Y-m-d', strtotime($data['tgl_nonaktif']));
    // $validasi            = validasi($data);
    $datas['publish'] = $data['publish'];
    // $datas['tgl_nonaktif'] = date('Y-m-d');
        try {
            $model = $db->update("testimoni", $datas, array('id' => $data['id']));
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
});


$app->get('/dashboard/artikel', function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("artikel.*, m_user.nama as penulis, artikel_kategori.nama as nama_kategori, artikel_kategori.id as id_kategori")
        ->from("artikel")
        ->join("LEFT JOIN","m_user","artikel.created_by = m_user.id")
        ->join("LEFT JOIN","artikel_kategori","artikel.kategori = artikel_kategori.id")
        ->orderBy("artikel.jam DESC")
        ->limit(5);

    $models    = $db->findAll();
    $totalItem = $db->count();


    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});