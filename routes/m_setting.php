<?php

function validasi($data, $custom = array())
{
    $validasi = array(
        "no_telepon"      => "required",
        "email"     =>  "required",
        "alamat"     =>  "required",
        "facebook"     =>  "required",
        "instagram"     =>  "required",
        "whatsapp"     =>  "required",
        "youtube"     =>  "required",
        "whatsapplink"     =>  "required"
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

$app->get("/m_setting/index", function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $setting = $db->select("*")
            ->from("setting")->find();
    return successResponse($response, $setting);
});

$app->post("/m_setting/save", function ($request, $response) {
    $params   = $request->getParams();
    $db       = $this->db;
    $validasi = validasi($params);

    if ($validasi !== true)
      return unprocessResponse($response, $validasi);

    try {
      // $params['waktu_pilih_unit'] = date("Y-m-d", strtotime($params['tanggal'])) . date(" H:i:s", strtotime($params['jam']));

      if (isset($params["id"])) {
          $model = $db->update("setting", $params, ["id" => $params["id"]]);
      } else {
          $model = $db->insert("setting", $params);
      }
      return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["Terjadi masalah pada server " . $e]);
    }

});
