<?php

$app->get('/migrasi/artikel', function ($request, $response) {
    set_time_limit(0);
    for ($i = 1; $i <= 1; $i++) {
        $string = file_get_contents(site_url() . "/migrasi/json_file/data" . $i . ".json");
        $data = json_decode($string, true);
        
        $artikel = [];
        foreach ($data['feed']['entry'] as $key => $value) {
        	$artikel[$key]['judul'] 			= $value['link'][2]['title'];
        	$artikel[$key]['alias'] 			= str_replace("https://www.wajibbaca.com/", "", $value['link'][2]['href']);
        	$artikel[$key]["created_at"]        = strtotime(date("Y-m-d H:i:s",strtotime($value['published']['$t'])));
        	$artikel[$key]["created_name"]      = $value['author'][0]['name']['$t'];
        	$artikel[$key]["description"] 		= getDescription($value['content']['$t']);
        	$artikel[$key]["gambar_primary"]    = !empty($value['media$thumbnail']['url']) ? str_replace("s72-c/", "", $value['media$thumbnail']['url']) : null;
        	$artikel[$key]["gambar_thumb"]      = !empty($value['media$thumbnail']['url']) ? str_replace("s72-c/", "", $value['media$thumbnail']['url']) : null;
        	$artikel[$key]['jam']				= strtotime(date("Y-m-d H:i:s",strtotime($value['published']['$t'])));
        	$keyword = [];
        	$is_viral = 0;
        	for ($i=0; $i < count($value['category']) ; $i++) { 
        		array_push($keyword, strtolower($value['category'][$i]['term']));
        		if ($value['category'][$i]['term'] == 'Featured') {
        			$is_viral = 1;
        		}
        	}
        	$artikel[$key]['keyword']			= $keyword;
        	$artikel[$key]['is_viral']			= $is_viral;
        	$artikel[$key]['content']			= $value['content']['$t'];
        }
    }
    return successResponse($response, ["list" => $artikel]);
});

// $app->post('/migrasi/mysql', function ($request, $response) {
//     $data               = $request->getParams();
//     $db                 = $this->db;

//     //$alias              = explode("/", $data['alias']);
//     $keyword            = join(", ", $data['keyword']);

//     //$data['status']     = "publish";
//     //$data['alias']      = $alias[2];
//     $data['keyword']    = $keyword;

//     $model = $db->insert("artikel_migrasi", $data);
//     // $data['detail'][0] = [
//     //     "artikel_id" => $model->id,
//     //     "content" => $data['judul'],
//     //     "posisi" => "judul"
//     // ];
//     //$detail = $db->insert("artikel_detail",$data['detail'][0]);
//     var_dump($data);exit();
// });