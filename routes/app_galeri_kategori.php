<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        'nama'       => 'required',
    );

    // GUMP::set_field_name("nama", "Nama Hak Akses");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * get user list
 */
$app->get('/app_galeri_kategori/index', function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("*")
        ->from('galeri_kategori')
        ->orderBy('galeri_kategori.id desc');

    /** set parameter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'nama') {
                $db->where('galeri_kategori.nama', 'LIKE', $val);
            }  else {
                $db->where($key, 'LIKE', $val);
            }
        }
    }

    /** Set limit */
    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    /** Set offset */
    if (isset($params['offset']) && !empty($params['offset'])) {
        $db->offset($params['offset']);
    }

    /** Set sorting */
    if (isset($params['sort']) && !empty($params['sort'])) {
        $db->orderBy($params['sort']);
    }

    $models    = $db->findAll();
    $totalItem = $db->count();
    // print_r($models);exit;


    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

/**
 * create user
 */
$app->post('/app_galeri_kategori/create', function ($request, $response) {
    $db   = $this->db;
    $data = json_decode(file_get_contents("php://input"), true);
    // print_r($data);exit;

    if (!empty($data['file_foto']['base64'])) {
            $folder = 'app/img/gambar_galeri_kategori/';
            if (!is_dir($folder)) {
                mkdir($folder, 0777);
            }
            $simpan_foto = base64ToFile($data['file_foto'], $folder);
            $temp        = explode(".", $simpan_foto["fileName"]);
            $newfilename = 'kategori-' .  $data['nama'] . randomImage(4) . '.' . end($temp);
            rename($simpan_foto['filePath'], $folder . $newfilename);

            $data['img'] = $newfilename;
        }


    $validasi = validasi($data);
    if ($validasi === true) {
        try {
            $model = $db->insert("galeri_kategori", $data);
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
    }
    return unprocessResponse($response, $validasi);
});

/**
 * update user
 */
$app->post('/app_galeri_kategori/update', function ($request, $response) {
    $data = json_decode(file_get_contents("php://input"), true);
    $db = $this->db;

    if (empty($data['file_foto']['base64'])) {
              unset($data['img']);
    } else {
        $folder = 'app/img/gambar_galeri_kategori/';
        //hapus foto lama
        $fotoLama = $db->select("img")->from("galeri_kategori")
            ->where("id", "=", $data['id'])->find();

        if (!empty($fotoLama->img)) {
            unlink($folder . $fotoLama->img);
        }

        //upload file foto yg baru
        if (!is_dir($folder)) {
            mkdir($folder, 0777);
        }

        $simpan_foto = base64ToFile($data['file_foto'], $folder);
        $temp        = explode(".", $simpan_foto["fileName"]);
        $newfilename = 'galeri-kategori-' .  $data['nama'] . randomImage(4) . '.' . end($temp);
        rename($simpan_foto['filePath'], $folder . $newfilename);

        $data['img'] = $newfilename;
    }

    $validasi = validasi($data);
    if ($validasi === true) {
        try {
          $model = $db->update("galeri_kategori", $data, array('id' => $data['id']));
          return successResponse($response, $model);

        } catch (Exception $e) {
          return unprocessResponse($response, $e);
        }
    }
    return unprocessResponse($response, $validasi);
});

$app->post('/app_galeri_kategori/hapusGambar', function ($request, $response) {
      $data = json_decode(file_get_contents("php://input"), true);
      $db = $this->db;
      $folder = 'app/img/gambar_galeri_kategori/';
      // print_r($data['id']);exit();
      $fotoLama = $db->select("img")->from("galeri_kategori")
      ->where("id", "=", $data['id'])->find();


      if (!empty($fotoLama->img)) {
        unlink($folder . $fotoLama->img);
      }

      $db->update('galeri_kategori', ['img' => ""], ['id' => $data['id']]);

      return successResponse($response, ['Berhasil Menghapus']);

});

$app->post('/app_galeri_kategori/trash', function ($request, $response) {

    $data = json_decode(file_get_contents("php://input"), true);
    // print_r($data);exit();
    $db                  = $this->db;
    // $data['tgl_nonaktif'] = date('Y-m-d', strtotime($data['tgl_nonaktif']));
    // $validasi            = validasi($data);
    $datas['is_deleted'] = $data['is_deleted'];
    // $datas['tgl_nonaktif'] = date('Y-m-d');
        try {
            $model = $db->update("galeri_kategori", $datas, array('id' => $data['id']));
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
});

/**
 * delete user
 */
$app->delete('/app_galeri_kategori/delete/{id}', function ($request, $response) {
    $sql = $this->db;
    $folder = 'app/img/gambar_galeri_kategori/';
      // print_r($data['id']);exit();
      $fotoLama = $sql->select("img")->from("galeri_kategori")
      ->where("id", "=", $request->getAttribute('id'))->find();

      if (!empty($fotoLama->img)) {
        unlink($folder . $fotoLama->img);
      }
    $delete = $sql->delete('galeri_kategori', array('id' => $request->getAttribute('id')));
    echo json_encode(array('status' => 1));
});
