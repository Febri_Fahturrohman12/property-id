<?php
 /**
 * Laporan Jumlah Artikel
 */
$app->get("/laporan/jumlahartikel", function ($request, $response) {
	$params = $request->getParams();
    $totalpublish   = 0;
    $totalviral     = 0;
    $totalall       = 0;
    $db = $this->db;
    $db->select("*")
       ->from("m_user")
       ->where("is_deleted","=",0)
       ->where("m_roles_id","!=",1);

    if (isset($params['nama'])) {
        $db->where("nama","LIKE",$params['nama']);
    }

    if (isset($params['tanggal'])) {
        $json = json_decode($params['tanggal']);
        $awal = strtotime(date("Y-m-d 00:00:00", strtotime($json->startDate)));
        $akhir = strtotime(date("Y-m-d 23:59:00", strtotime($json->endDate)));
    }

    $model = $db->findAll();
    foreach ($model as $key => $val) {
        $model[$key] = (array)$val;
        $publish = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE status = 'publish' AND is_viral = 0 AND created_by =".$model[$key]['id']." AND created_at >= ".$awal." AND created_at <= ".$akhir);

        $viral   = $db->find("SELECT count(*) AS jumlah FROM artikel WHERE status = 'publish' AND is_viral = 1 AND created_by =".$model[$key]['id']." AND created_at >= ".$awal." AND created_at <= ".$akhir);

        $model[$key]["publish"] = $publish->jumlah;
        $model[$key]["viral"] = $viral->jumlah;
        $model[$key]["total"] = $publish->jumlah + $viral->jumlah;
        $totalviral += $viral->jumlah;
        $totalpublish += $publish->jumlah;
        $totalall = $totalviral + $totalpublish;
    }
    return successResponse($response, [
        "list" => $model,
        "periode" => [
            "awal" => date("Y-m-d",$awal),
            "akhir"=>date("Y-m-d",$akhir)
        ],
        "total" => [
            "all" => $totalall,
            "publish" => $totalpublish,
            "viral" => $totalall
        ]
    ]);
});
 /**
 * Laporan Bulanan
 */
$app->get("/laporan/bulanan", function ($request, $response) {
    $params  = $request->getParams();
    $db      = $this->db;
    $periode = date("M - Y",strtotime($params['tanggal']));

    //Queri
    $db->select("*")->from("m_user")->where("is_deleted","=",0)->where("m_roles_id","!=",1);

    //Fillter
    if (isset($params['nama'])) {
        $db->where("nama","LIKE",$params['nama']);
    }
    if (isset($params['tanggal'])) {
        $bulan = date("m",strtotime($params['tanggal']));
        $tahun = date("Y",strtotime($params['tanggal']));
        $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
    }

    $keseluruhan = 0;
    $model2 = [];

    $model = $db->findAll();
    foreach ($model as $key => $val) {
        $model[$key] = (array)$val;
        $model[$key]['total'] = 0;
        for ($i = 1; $i <= $tanggal; $i++) {
            if ($i < 10) {
                $hariini = strtotime(date("Y-m-0{$i} 00:00:00",strtotime($params['tanggal'])));    
            } else {
                $hariini = strtotime(date("Y-m-{$i} 00:00:00",strtotime($params['tanggal'])));   
            }
            $besok = strtotime("+1 day", $hariini);

            $model[$key]['detail'][$i] = $db->select("count(*) as jumlah")->from("artikel")->where("status","=","publish")->where("created_at",">=",$hariini)->where("created_at","<=",$besok)->where("created_by","=",$model[$key]['id'])->find();

            $model[$key]['total'] += $model[$key]['detail'][$i]->jumlah;
        }
        $keseluruhan += $model[$key]['total'];
    }

    for ($i = 1; $i <= $tanggal; $i++) {
        if ($i < 10) {
            $hariini = strtotime(date("Y-m-0{$i} 00:00:00",strtotime($params['tanggal'])));    
        } else {
            $hariini = strtotime(date("Y-m-{$i} 00:00:00",strtotime($params['tanggal'])));   
        }
        $besok = strtotime("+1 day", $hariini);

        $model2[$i] = $db->select("count(*) as jumlah")->from("artikel")->where("status","=","publish")->where("created_at",">=",$hariini)->where("created_at","<=",$besok)->find();
    }
    return successResponse($response, [
        "list" => $model,
        "tanggal" => $tanggal,
        "periode" => $periode,
        "keseluruhan" => $keseluruhan,
        "totalbawah" => $model2
    ]);
});
 /**
 * Ambil Tahun
 */
$app->get('/laporan/tahun', function($request, $response) {
    $tahun_awal = 2015;
    $tahunSekarang = date("Y") + 20;
    $thn_awal = [];
    $a = 0;
    for ($i = $tahun_awal; $i <= $tahunSekarang; $i++) {
        $thn_awal[$a] = $i;
        $a++;
    }

    $models = [];
    $c = 0;
    for ($i = 0; $i < count($thn_awal); $i++) {
        $models[$c] = $thn_awal[$i];
        $c++;
    }
    return successResponse($response, ["list" => $models]);
});

