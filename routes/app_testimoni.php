<?php

/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        'nama'       => 'required',
    );

    // GUMP::set_field_name("artikel_kategori_id", "Kategori Artikel");
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * get user list
 */
$app->get('/app_testimoni/index', function ($request, $response) {
    $params = $request->getParams();
    $db     = $this->db;

    $db->select("*")
        ->from('testimoni')
        ->orderBy('testimoni.id desc');

    /** set parameter */
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            if ($key == 'nama') {
                $db->where('testimoni.nama', 'LIKE', $val);
            } else if ($key == 'is_deleted') {
                $db->where('testimoni.is_deleted', '=', $val);
            } else {
                $db->where($key, 'LIKE', $val);
            }
        }
    }

    /** Set limit */
    if (isset($params['limit']) && !empty($params['limit'])) {
        $db->limit($params['limit']);
    }

    /** Set offset */
    if (isset($params['offset']) && !empty($params['offset'])) {
        $db->offset($params['offset']);
    }

    /** Set sorting */
    if (isset($params['sort']) && !empty($params['sort'])) {
        $db->orderBy($params['sort']);
    }

    $models    = $db->findAll();
    $totalItem = $db->count();
    // print_r($models);exit;


    return successResponse($response, ['list' => $models, 'totalItems' => $totalItem]);
});

$app->get("/app_testimoni/generate_kode", function($request, $response){
  $db = $this->db;
  $model  = $db->find("SELECT MAX(kode) as kode FROM testimoni");
  $kode   = generate_kode("VCR", $model->kode);
  return json_encode(['kode' => $kode]);
});

$app->get('/app_testimoni/get_customer', function ($request, $response) {
    $db = $this->db;

    $data = $db->select('*')
                ->from('m_customer')
                ->findAll();
                // print_r($data);exit();

    return successResponse($response, $data);
});

/**
 * create user
 */
$app->post('/app_testimoni/create', function ($request, $response) {
  $db   = $this->db;
  $data = json_decode(file_get_contents("php://input"), true);
  // print_r($data);exit();
  if (!empty($data['file_foto']['base64'])) {
      $folder = 'app/img/gambar_testimoni/';
      if (!is_dir($folder)) {
          mkdir($folder, 0777);
      }
      $simpan_foto = base64ToFile($data['file_foto'], $folder);
      $temp        = explode(".", $simpan_foto["fileName"]);
      $newfilename = 'Image' .  $data['nama']. '-' . random(4) . '.' . end($temp);
      rename($simpan_foto['filePath'], $folder . $newfilename);

      $data['img'] = $newfilename;
  }

  $validasi = validasi($data);
  if ($validasi === true) {
      try {
          $model = $db->insert("testimoni", $data);
          return successResponse($response, $model);
      } catch (Exception $e) {
          return unprocessResponse($response, ['data gagal disimpan']);
      }
  }
  return unprocessResponse($response, $validasi);
});

/**
 * update user
 */
$app->post('/app_testimoni/update', function ($request, $response) {
  $data = json_decode(file_get_contents("php://input"), true);
  $db = $this->db;

  if (empty($data['file_foto']['base64'])) {
              unset($data['img']);
    } else {
        $folder = 'app/img/gambar_testimoni/';
        //hapus foto lama
        $fotoLama = $db->select("img")->from("testimoni")
            ->where("id", "=", $data['id'])->find();

        if (!empty($fotoLama->img)) {
            unlink($folder . $fotoLama->img);
        }

        //upload file foto yg baru
        if (!is_dir($folder)) {
            mkdir($folder, 0777);
        }

        $simpan_foto = base64ToFile($data['file_foto'], $folder);
        $temp        = explode(".", $simpan_foto["fileName"]);
        $newfilename = 'Image' .  $data['nama']. '-' . randomImage(4) . '.' . end($temp);
        rename($simpan_foto['filePath'], $folder . $newfilename);

        $data['img'] = $newfilename;
    }

  $validasi = validasi($data);
  if ($validasi === true) {
      try {
        $model = $db->update("testimoni", $data, array('id' => $data['id']));
        return successResponse($response, $model);

      } catch (Exception $e) {
        return unprocessResponse($response, $e);
      }
  }
  return unprocessResponse($response, $validasi);

});

$app->post('/app_testimoni/hapusGambar', function ($request, $response) {
      $data = json_decode(file_get_contents("php://input"), true);
      $db = $this->db;
      $folder = 'app/img/gambar_testimoni/';
      // print_r($data['id']);exit();
      $fotoLama = $db->select("img")->from("testimoni")
      ->where("id", "=", $data['id'])->find();


      if (!empty($fotoLama->img)) {
        unlink($folder . $fotoLama->img);
      }

      $db->update('testimoni', ['img' => ""], ['id' => $data['id']]);

      return successResponse($response, ['Berhasil Menghapus']);

});

$app->post('/app_testimoni/trash', function ($request, $response) {

    $data = json_decode(file_get_contents("php://input"), true);
    // print_r($data);exit();
    $db                  = $this->db;
    // $data['tgl_nonaktif'] = date('Y-m-d', strtotime($data['tgl_nonaktif']));
    // $validasi            = validasi($data);
    $datas['is_deleted'] = $data['is_deleted'];
    // $datas['tgl_nonaktif'] = date('Y-m-d');
        try {
            $model = $db->update("testimoni", $datas, array('id' => $data['id']));
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
});

$app->post('/app_testimoni/is_approve', function ($request, $response) {

    $data = json_decode(file_get_contents("php://input"), true);
    // print_r($data);exit();
    $db                  = $this->db;
    // $data['tgl_nonaktif'] = date('Y-m-d', strtotime($data['tgl_nonaktif']));
    // $validasi            = validasi($data);
    $datas['publish'] = $data['publish'];
    // $datas['tgl_nonaktif'] = date('Y-m-d');
        try {
            $model = $db->update("testimoni", $datas, array('id' => $data['id']));
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ['data gagal disimpan']);
        }
});

/**
 * delete user
 */
$app->delete('/app_testimoni/delete/{id}', function ($request, $response) {
    $sql = $this->db;
    $folder = 'app/img/gambar_testimoni/';
    $fotoLama = $sql->select("img")->from("testimoni")
    ->where("id", "=", $request->getAttribute('id'))->find();


    if (!empty($fotoLama->img)) {
      unlink($folder . $fotoLama->img);
    }
    
    $delete = $sql->delete('testimoni', array('id' => $request->getAttribute('id')));
    echo json_encode(array('status' => 1));
});
