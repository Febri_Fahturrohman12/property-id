<?php
/**
 * Ambil session user
 */
$app->get('/site/session', function ($request, $response) {
    if (isset($_SESSION['user']['m_roles_id'])) {
        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['undefined']);
})->setName('session');
/**
 * Proses login
 */
$app->post('/site/login', function ($request, $response) {
    $params   = $request->getParams();
    $sql      = $this->db;
    $email = isset($params['email']) ? $params['email'] : '';
    $password = isset($params['password']) ? $params['password'] : '';
    /**
     * Login Admin
     */
    $sql->select("m_user.*, m_roles.akses, m_roles.nama as status")
        ->from("m_user")
        ->leftJoin("m_roles", "m_roles.id = m_user.m_roles_id")
        ->where("email", "=", $email)
        ->andWhere("password", "=", sha1($password));
    $model = $sql->find();
    /**
     * Simpan user ke dalam session
     */
    if (isset($model->id)) {
        $_SESSION['user']['id']         = $model->id;
        $_SESSION['user']['email']      = $model->email;
        $_SESSION['user']['nama']       = $model->nama;
        $_SESSION['user']['status']     = $model->status;
        $_SESSION['user']['m_roles_id'] = $model->m_roles_id;
        $_SESSION['user']['akses']      = json_decode($model->akses);
        

        return successResponse($response, $_SESSION);
    }
    return unprocessResponse($response, ['Authentication Systems gagal, email atau password Anda salah.']);
})->setName('login');
/**
 * Hapus semua session
 */
$app->get('/site/logout', function ($request, $response) {
    session_destroy();
    return successResponse($response, []);
})->setName('logout');

$app->get('/site/artikel', function ($request, $response) {
    $data = [];
    $sql      = $this->db;

    try {
        $artikelPenulis = [
            "All"           => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE created_by =".$_SESSION['user']['id']),
            "Editorial"     => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'editorial' AND created_by =".$_SESSION['user']['id']),
            "Draf"          => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'draf' AND created_by =".$_SESSION['user']['id']),
            "Published"     => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'publish' AND created_by =".$_SESSION['user']['id']),
            "Revisi"        => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'revisi' AND created_by =".$_SESSION['user']['id']),
            "Ditolak"       => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'tolak' AND created_by =".$_SESSION['user']['id']),
            "Trash"         => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'trash' AND created_by =".$_SESSION['user']['id']),
        ];

        $artikelEditor = [
            "All"           => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE modified_by =".$_SESSION['user']['id']),
            "Editorial"     => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'editorial' AND modified_by =".$_SESSION['user']['id']),
            "Draf"          => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'draf' AND modified_by =".$_SESSION['user']['id']),
            "Published"     => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'publish' AND modified_by =".$_SESSION['user']['id']),
            "Revisi"        => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'revisi' AND modified_by =".$_SESSION['user']['id']),
            "Ditolak"       => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'tolak' AND modified_by =".$_SESSION['user']['id']),
            "Trash"         => $sql->find("SELECT COUNT(*) AS jumlah FROM artikel WHERE status = 'trash' AND modified_by =".$_SESSION['user']['id']),
        ];

        if ($_SESSION['user']['status'] == 'Penulis') {
            $data   = $artikelPenulis;
        } else {
            $data  = $artikelEditor;
        }

        return successResponse($response, ["list" => $data]);
    } catch (Exception $e) {
      return unprocessResponse($response, ["Terjadi masalah pada server"]);
    }
})->setName('Artikel');


/** UPLOAD GAMBAR CKEDITOR */
$app->post('/site/upload.html', function ($request, $response) {
    $files = $request->getUploadedFiles();
    $newfile = $files['upload'];

    if (file_exists("file/" . $newfile->getClientFilename())) {
        echo $newfile->getClientFilename() . " already exists please choose another image.";
    } else {

        $path = 'app/img/artikel/' . date("m-Y") . '/';
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }

        $uploadFileName = urlParsing($newfile->getClientFilename());
        $upload = $newfile->moveTo($path . $uploadFileName);

        // $crtImg = createImgArtikel($path . '/', $uploadFileName, date("dYh"), true);

        // $path2 = 'img/artikel/' . date("m-Y") . '/';
        // $url = site_url() . $path . $crtImg['big'];
        $url = site_url() . $path . $uploadFileName;
        // print_r($url);die();
        // Required: anonymous function reference number as explained above.
        // $funcNum = $_POST['CKEditorFuncNum'];
        // Optional: instance name (might be used to load a specific configuration file or anything else).
        // $CKEditor = $_POST['CKEditor'];
        // Optional: might be used to provide localized messages.
        // $langCode = $_POST['langCode'];

        echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(1, '$url', '');</script>";
    }
});

$app->get('/site/migrasiKonten', function ($request, $response) {
    $sql      = $this->db;

    $models = $sql->select("*")->from("artikel")->findAll();

    // foreach ($models as $key => $value) {
    //     $models[$key] = (array) $value;
    //     $models[$key]['isi_content'] = str_replace("http://", "https://", $value->isi_content);

    //     $update = $sql->update("artikel",["isi_content"=>$models[$key]['isi_content']],["id"=>$value->id]);
    // }

    // echo "berhasil";
    print_r($models);
})->setName('session');