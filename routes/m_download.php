<?php
/**
 * Validasi
 * @param  array $data
 * @param  array $custom
 * @return array
 */

/**
 * Validasi Data Peminat
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "nama"       => "required",
        "keterangan"     =>  "required",
        "status"     => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}


/**
 * Ambil semua m_user
 */

$app->get("/m_download/index", function ($request, $response) {
    $params = $request->getParams();
    // print_r($params);die();
    $db     = $this->db;
            $db->select("*")
            ->from("m_download");

    /**
    * Filter
    */
    if (isset($params["filter"])) {
        $filter = (array) json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
        foreach ($filter as $key => $val) {
            if ($key == 'nama') {
               $db->where('m_download.nama', 'LIKE', $val);
            } elseif ($key == 'status') {
                $db->andWhere('m_download.is_deleted', '=', $val);               
            }else if($key == 'jenis'){
                $db->andWhere('m_download.jenis', '=', $val);
            }
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models    = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});





/**
 * Save Peminat
 */
$app->post("/m_download/save", function ($request, $response) {
    $data = $request->getParams();
    $file_path        = __DIR__ . "";
    $file_path        = substr($file_path, 0, strpos($file_path, "app")) . "assets/" . "file_download/";
    $db   = $this->db;
    $validasi         = validasi($data);
    
    if ($validasi === true) {
        try {
            // Upload site pdf
            if (isset($data['upload_file']) && is_array($data['upload_file'])) {
                $simpan_file = base64ToFile($data['upload_file'], $file_path);
                $temp        = explode(".", $simpan_file["fileName"]);
                $newfilename = $simpan_file["fileName"];
                // . end($temp)
                rename($simpan_file['filePath'], $file_path . $newfilename);
                $data['upload_file'] = $simpan_file["fileName"];
            }

            if (isset($data["id"])) {
                // print_r($data);die();
                $model = $db->update("m_download", $data, ["id" => $data["id"]]);
            } else {
                $model = $db->insert("m_download", $data);
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["Mohon maaf terjadi kesalahan mohon periksa kembali data anda"]);
        }
    }
    return unprocessResponse($response, $validasi);
});



/**
 * Hapus m_user
 */
$app->post("/m_download/delete", function ($request, $response) {
    $data     = $request->getParams();
    $db       = $this->db;
    try {
        $model = $db->delete("m_download", ["id" => $data["id"]]);
                return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
